package service

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type WeatherCon struct {
	Date       string `json:"date"`
	Week       string `json:"week"`
	City       string `json:"city"`
	Wea        string `json:"wea"`
	TemDay     string `json:"tem_day"`
	Tem        string `json:"tem"`
	TemNight   string `json:"tem_night"`
	Win        string `json:"win"`
	WinSpeed   string `json:"win_speed"`
	WinMeter   string `json:"win_meter"`
	Air        string `json:"air"`
	Pressure   string `json:"pressure"`
	Humidity   string `json:"humidity"`
	UpdateTime string `json:"update_time"`
}

// GetByWeather 获取天气预报
func GetByWeather(city string) WeatherCon {
	var weather = WeatherCon{}
	url := fmt.Sprintf("https://v1.yiketianqi.com/free/day?appid=%s&appsecret=%s&city=%s&unescape=1", WeatherAppid, WeatherAppSecret, city)
	fmt.Println(url)
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("获取天气失败", err)
		return weather
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("读取内容失败", err)
		return weather
	}

	err = json.Unmarshal(body, &weather)
	if err != nil {
		fmt.Println("天气解析json失败")
		return weather
	}
	return weather
}
