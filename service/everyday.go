package service

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/tidwall/gjson"
)

type EverydayCon struct {
	Content     string `json:"content"`
	Note        string `json:"note"`
	Translation string `json:"translation"`
}

func GetByEveryday() (EverydayCon, string) {
	var everyday = EverydayCon{}
	// 星座运势
	//https://api.tianapi.com/star/index?key=317dcef343a561130b81a950990603eb&astro=%E5%8F%8C%E9%B1%BC%E5%BA%A7
	resp, err := http.Get("https://open.iciba.com/dsapi/?date")
	if err != nil {
		fmt.Println("获取每日一句失败", err)
		return everyday, ""
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("读取内容失败", err)
		return everyday, ""
	}

	err = json.Unmarshal(body, &everyday)
	if err != nil {
		fmt.Println("每日一句解析json失败")
		return everyday, ""
	}

	shareUrl := gjson.Get(string(body), "fenxiang_img").String()
	return everyday, shareUrl
}
