package service

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

// PostTemplate 发送模板消息
func PostTemplate(accessToken string, reqdata string, templateID string, openid string) {

	fmt.Println("accessToken:", accessToken)
	fmt.Println("reqdata:", reqdata)
	fmt.Println("templateID:", templateID)
	fmt.Println("openid:", openid)

	url := "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + accessToken

	reqbody := "{\"touser\":\"" + openid + "\", \"template_id\":\"" + templateID + "\", \"data\": " + reqdata + "}"

	resp, err := http.Post(url,
		"application/x-www-form-urlencoded",
		strings.NewReader(string(reqbody)))
	if err != nil {
		fmt.Println(err)
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(openid, string(body))
}
