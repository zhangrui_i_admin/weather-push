module waether-push

go 1.22rc2

require github.com/tidwall/gjson v1.17.1

require (
	github.com/robfig/cron/v3 v3.0.1
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
)
